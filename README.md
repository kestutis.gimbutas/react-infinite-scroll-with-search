# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Spaceflight News

Implementation of infinite scroll with search of [Spaceflight News API](https://api.spaceflightnewsapi.net/v4/docs/#/articles/articles_list).

### `Technologies used`

- Typescript
- SWR Custom hook
- Styled-components

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

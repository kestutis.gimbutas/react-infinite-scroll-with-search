import { useState, useEffect } from "react";
import styled from "styled-components";
import "./App.css";
import InputSearchDebounced from "./components/InputSearchDebounced";
import ContainerOfArticles from "./components/ContainerOfArticles";
import useSwrInfiniteSpaceFlightNewsApiArticles from "./hooks/useSwrInfiniteSpaceFlightNewsApiArticles";

const H1 = styled.h1`
  color: #ffffe0;
  font-size: 2rem;
`;

function App() {
  const [inputSearchDebounced, setInputSearchDebounced] = useState("");
  const articlesPerPage = 12;

  const { data, isLoading, isError, size, setSize } =
    useSwrInfiniteSpaceFlightNewsApiArticles(
      articlesPerPage,
      inputSearchDebounced
    );

  useEffect(() => {
    const handleScroll = () => {
      if (
        window.innerHeight + document.documentElement.scrollTop ===
          document.documentElement.offsetHeight &&
        !isLoading
      ) {
        setSize(size + 1);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  return (
    <div className="App">
      <H1>☆ ⭒ Articles of Space Flights ⭒ ☆</H1>
      <InputSearchDebounced setInputDebounced={setInputSearchDebounced} />
      <ContainerOfArticles isLoading={isLoading} articles={data} />
      {isError && <div>ERROR: {isError}</div>}
    </div>
  );
}

export default App;

export type Article = {
  id: number;
  title: string;
  image_url: string;
  url: string;
  news_site: string;
};

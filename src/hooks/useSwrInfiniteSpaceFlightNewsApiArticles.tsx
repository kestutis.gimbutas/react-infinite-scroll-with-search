import { useMemo } from "react";
import useSWRInfinite from "swr/infinite";
import { Article } from "../types/types";

type Returns = {
  data: Article[];
  isLoading: boolean;
  isError: string;
  size: number;
  setSize: any;
};

const useSwrInfiniteSpaceFlightNewsApiArticles = (
  articlesPerPage: number,
  search: string | undefined
): Returns => {
  const getKey = (pageIndex: number | undefined, previousPageData: any) => {
    if (previousPageData && !previousPageData.results) return null;
    if (pageIndex === 0)
      return `https://api.spaceflightnewsapi.net/v4/articles?offset=0&limit=${articlesPerPage}&search=${
        search ? search : ""
      }`;
    return previousPageData?.next;
  };

  const fetcher = (url: string) => fetch(url).then((res) => res.json());

  const { data, error, isLoading, size, setSize } = useSWRInfinite(
    getKey,
    fetcher
  );

  const articles: Article[] = useMemo(() => {
    let results: Article[] = [];
    if (data) {
      data.forEach((data) => {
        results = [...results, ...data.results];
      });
    }
    return results;
  }, [data]);

  return {
    data: articles,
    isLoading,
    isError: error,
    size: size,
    setSize: setSize,
  };
};

export default useSwrInfiniteSpaceFlightNewsApiArticles;

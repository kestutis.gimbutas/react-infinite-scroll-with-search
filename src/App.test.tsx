import React from "react";
import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders search button", () => {
  render(<App />);
  const searchBtn = screen.getByText(/Search/i);
  expect(searchBtn).toBeInTheDocument();
});

import { memo } from "react";
import CardOfArticle from "./CardOfArticle";
import styled from "styled-components";
import { Article } from "../types/types";

const Ul = styled.ul`
  min-height: 90vh;
  background: linear-gradient(
    0deg,
    rgba(255, 255, 255, 1) 0%,
    rgba(0, 127, 255, 1) 20%,
    rgba(0, 0, 0, 1) 100%
  );
`;

const Div = styled.div`
  color: #ffffe0;
  font-size: 2rem;
  margin: 0;
`;

type props = {
  articles: Article[];
  isLoading: boolean;
};

const ContainerOfArticles = memo((props: props) => {
  const { isLoading, articles } = props;
  return (
    <Ul>
      {articles.length ? (
        articles.map((articleInfo: Article) => {
          return <CardOfArticle articleInfo={articleInfo} />;
        })
      ) : (
        <Div>{isLoading ? "Loading" : "No articles found"}</Div>
      )}
    </Ul>
  );
});

export default ContainerOfArticles;

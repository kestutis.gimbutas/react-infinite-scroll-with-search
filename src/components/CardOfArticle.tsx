import styled from "styled-components";
import { Article } from "../types/types";

const Li = styled.li`
  color: black;
  display: inline-block;
  width: 20rem;
  height: 20rem;
  margin: 1rem;
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
  margin-right: 10px;
  border-radius: 5px;
  object-fit: cover;
`;

const H2 = styled.h2`
  font-size: 1rem;
  margin: 0;
`;

const A = styled.a`
  text-decoration: none;
  color: #fa8072;
  transition: color 0.3s ease;
  &:hover {
    color: #ffffe0;
  }
`;

const Div = styled.div`
  font-size: 1rem;
  color: #696969;
  &:hover {
    color: #fffacd;
  }
`;

type ArticleProp = {
  articleInfo: Article;
};

export default function CardOfArticle(props: ArticleProp) {
  const { id, title, image_url, url, news_site } = props.articleInfo;
  return (
    <Li key={id}>
      <A href={url} target="_blank" rel="noopener noreferrer">
        <Img src={image_url} alt={news_site} />
        <H2>{title}</H2>
        <Div>by {news_site}</Div>
      </A>
    </Li>
  );
}

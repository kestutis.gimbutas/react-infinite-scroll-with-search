import { useState, useEffect } from "react";
import styled from "styled-components";

const Input = styled.input`
  padding: 10px;
  border: 2px solid #ccc;
  border-radius: 5px;
  font-size: 16px;
  width: 40%;
  transition: border-color 0.3s ease;

  &:focus {
    border-color: #007bff;
    outline: none;
  }
`;

let timeoutId: any;

const InputSearchDebounced = (props: any) => {
  const inputDelayMs = 1000;
  const [inputSearch, setInputSearch] = useState("");

  useEffect(() => {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => {
      props.setInputDebounced(inputSearch);
    }, inputDelayMs);
  }, [inputSearch]);

  return (
    <>
      <Input
        onChange={(e) => setInputSearch(e.target.value)}
        value={props.value}
        placeholder={"Search..."}
        type="text"
      />
    </>
  );
};

export default InputSearchDebounced;
